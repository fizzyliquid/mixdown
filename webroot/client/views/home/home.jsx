import React, { Component } from 'react';
import './home.css';
import { Container, Title, Box } from './home.styles';

class Home extends Component {
  render() {
    return (
      <Box>
        <Title> Fizzy Beats </Title>
      </Box>
    );
  };
};

export default Home;